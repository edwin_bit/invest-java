package net.invest.gradle.client;

import org.apache.log4j.Logger;

import net.service.gradle.core.MessageService;

/**
 * @author Edwin Esquivel
 */
public class HelloWorld {

    private static final Logger LOGGER = Logger.getLogger(HelloWorld.class);

    public static void main(String[] args) {
        MessageService messageService = new MessageService();

        String message = messageService.getMessage();
        LOGGER.info("Received message: " + message);
    }
}
