package com.core.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.core.service.EmployeeService;
import com.invest.models.Employee;

@Controller
public class EmployeeController {
	
	private static final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);
	EmployeeService employeeService = new EmployeeService();
	private List<Employee> employees;

//	// Good one
//	@RequestMapping(value = "/employee", method = RequestMethod.GET)
//	public String index(Map<String, Object> model) {		
//		
//		LOG.info("employee is executed!");		
//		EmployeeService employeeService = new EmployeeService();
//		model.put("employeeList", employeeService.getAllEmployees());
//				
//		return "list-employees";
//	}
	
	
	// Good one two
//	@RequestMapping(value = "/employee")
//	public String employee(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
//		LOG.info("employee is executed!");		
//		String action = request.getParameter("searchAction");
//		
//	      if (action!=null){
//	    	  return "list-employees";
//		  }else{			
//			EmployeeService employeeService = new EmployeeService();
//			model.put("employeeList", employeeService.getAllEmployees());
//					
//			return "list-employees";
//		  }		
//	}

//	// http://localhost:8080/employee?searchAction=searchByName&employeeName=adams
	
	
	
	@RequestMapping(value = "/employee", method = {RequestMethod.GET, RequestMethod.POST})
	public String employee(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		LOG.info("employee get is executed!");
		String action = request.getParameter("searchAction");
		if (action != null) {
			String employeeName = request.getParameter("employeeName");
			String id = request.getParameter("idEmployee");
			long employeeId = Integer.valueOf(id == null ? "0" : id);
			if (employeeName != null) {
				this.employees = this.searchEmployeeByName(employeeName);
				model.put("employeeList", employees);
			} else if (employeeId > 0) {
				Employee employee = null;
				try {
					employee = this.searchEmployeeById(employeeId);
					this.employees.clear();
					this.employees.add(employee);
					model.put("employeeList", employees);
				} catch (Exception e) {
					LOG.info(e.getMessage());
				}
			}
		} else {
			if (!model.containsKey("employeeList")) {
				this.employees = this.employeeService.getAllEmployees();
				model.put("employeeList", employees);				
			}
		}
		return "list-employees";
	}
	
	@RequestMapping(value = "/new-employee", method = RequestMethod.GET)
	public String addEmploy(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		LOG.info("employee post is executed!");
		String action = request.getParameter("action");
		if (action != null) {
			switch (action) {
			case "add":
				this.employees = addEmployeeAction(request);
				model.put("employeeList", employees);
				return "list-employees";
			case "edit":
//				editEmployeeAction(req, resp);
				break;
			case "remove":
//				removeEmployeeByName(req, resp);
				break;
			}
		} else {
//			employees = this.employeeService.getAllEmployees();
//			model.put("employeeList", employees);
		}
		return "new-employee";
	}
	
    private List<Employee> addEmployeeAction(HttpServletRequest request) {
        String name = request.getParameter("name");
        String lastName = request.getParameter("lastName");
        String birthday = request.getParameter("birthDate");
        String role = request.getParameter("role");
        String department = request.getParameter("department");
        String email = request.getParameter("email");
        Employee employee = new Employee(name, lastName, birthday, role, department, email);
        long idEmployee = this.employeeService.addEmployee(employee);
        request.setAttribute("idEmployee", idEmployee);
        String message = "The new employee has been successfully created.";
        request.setAttribute("message", message);
        return employeeService.getAllEmployees();
    }
	
	private List<Employee> searchEmployeeByName(String employeeName) {
		return this.employeeService.searchEmployeesByName(employeeName);
	}
	
	private Employee searchEmployeeById(long employeeId) throws Exception {
		return this.employeeService.getEmployee(employeeId);
    }
}



//package com.core.web;
//
//import java.io.IOException;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//import javax.servlet.RequestDispatcher;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import com.core.service.EmployeeService;
//import com.invest.controller.EmployeeServlet;
//import com.invest.models.Employee;
//
//@WebServlet(
//        name = "EmployeeServlet",
//        urlPatterns = {"/employee"}
//)
//public class EmployeeController extends HttpServlet {
//
//    /**
//	 * 
//	 */
//	private static final long serialVersionUID = -8390250650709892810L;
//	EmployeeService employeeService = new EmployeeService();
//
//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String action = req.getParameter("searchAction");
//        if (action!=null){
//            switch (action) {           
//            case "searchById":
//                searchEmployeeById(req, resp);
//                break;           
//            case "searchByName":
//                searchEmployeeByName(req, resp);
//                break;
//            }
//        }else{
//            List<Employee> result = employeeService.getAllEmployees();
//            forwardListEmployees(req, resp, result);
//        }
//    }
//
//    private void searchEmployeeById(HttpServletRequest req, HttpServletResponse resp)
//            throws ServletException, IOException {
//        long idEmployee = Integer.valueOf(req.getParameter("idEmployee"));
//        Employee employee = null;
//        try {
//            employee = employeeService.getEmployee(idEmployee);
//        } catch (Exception ex) {
//            Logger.getLogger(EmployeeServlet.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        req.setAttribute("employee", employee);
//        req.setAttribute("action", "edit");
//        String nextJSP = "new-employee.jsp";
//        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
//        dispatcher.forward(req, resp);
//    }
//    
//    private void searchEmployeeByName(HttpServletRequest req, HttpServletResponse resp)
//            throws ServletException, IOException {
//        String employeeName = req.getParameter("employeeName");
//        List<Employee> result = employeeService.searchEmployeesByName(employeeName);        
//        forwardListEmployees(req, resp, result);
//    }
//
//    private void forwardListEmployees(HttpServletRequest req, HttpServletResponse resp, List<Employee> employeeList)
//            throws ServletException, IOException {
//        String nextJSP = "list-employees.jsp";
//        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
//        req.setAttribute("employeeList", employeeList);
//        dispatcher.forward(req, resp);
//    }   
//    
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
//            throws ServletException, IOException {
//        String action = req.getParameter("action");
//        switch (action) {
//            case "add":
//                addEmployeeAction(req, resp);
//                break;
//            case "edit":
//                editEmployeeAction(req, resp);
//                break;            
//            case "remove":
//                removeEmployeeByName(req, resp);
//                break;            
//        }
//
//    }
//
//    private void addEmployeeAction(HttpServletRequest req, HttpServletResponse resp)
//            throws ServletException, IOException {
//        String name = req.getParameter("name");
//        String lastName = req.getParameter("lastName");
//        String birthday = req.getParameter("birthDate");
//        String role = req.getParameter("role");
//        String department = req.getParameter("department");
//        String email = req.getParameter("email");
//        Employee employee = new Employee(name, lastName, birthday, role, department, email);
//        long idEmployee = employeeService.addEmployee(employee);
//        List<Employee> employeeList = employeeService.getAllEmployees();
//        req.setAttribute("idEmployee", idEmployee);
//        String message = "The new employee has been successfully created.";
//        req.setAttribute("message", message);
//        forwardListEmployees(req, resp, employeeList);
//    }
//
//    private void editEmployeeAction(HttpServletRequest req, HttpServletResponse resp)
//            throws ServletException, IOException {
//        String name = req.getParameter("name");
//        String lastName = req.getParameter("lastName");
//        String birthday = req.getParameter("birthDate");
//        String role = req.getParameter("role");
//        String department = req.getParameter("department");
//        String email = req.getParameter("email");
//        long idEmployee = Integer.valueOf(req.getParameter("idEmployee"));
//        Employee employee = new Employee(name, lastName, birthday, role, department, email, idEmployee);
//        employee.setId(idEmployee);
//        boolean success = employeeService.updateEmployee(employee);
//        String message = null;
//        if (success) {
//            message = "The employee has been successfully updated.";
//        }
//        List<Employee> employeeList = employeeService.getAllEmployees();
//        req.setAttribute("idEmployee", idEmployee);
//        req.setAttribute("message", message);
//        forwardListEmployees(req, resp, employeeList);
//    }  
//
//    private void removeEmployeeByName(HttpServletRequest req, HttpServletResponse resp)
//            throws ServletException, IOException {
//        long idEmployee = Integer.valueOf(req.getParameter("idEmployee"));
//        boolean confirm = employeeService.deleteEmployee(idEmployee);
//        if (confirm){
//            String message = "The employee has been successfully removed.";
//            req.setAttribute("message", message);
//        }
//        List<Employee> employeeList = employeeService.getAllEmployees();
//        forwardListEmployees(req, resp, employeeList);
//    }
//
//}
