package net.invest.gradle.coe;

import org.junit.Before;
import org.junit.Test;

import net.service.gradle.core.MessageService;

import static org.junit.Assert.assertEquals;

/**
 * @author Edwin Esquivel
 */
public class MessageServiceTest {

    private MessageService messageService;

    @Before
    public void setUp() {
        messageService = new MessageService();
    }

    @Test
    public void getMessage_ShouldReturnMessage() {
        assertEquals("from service Hello World!", messageService.getMessage());
    }
}
